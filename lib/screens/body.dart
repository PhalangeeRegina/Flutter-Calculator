import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Column(
      children: [
        Container(
          child: Row(children: <Widget>[
            Expanded(
              child: FlatButton(
                onPressed: () {},
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 0,
                  ),
                  decoration: BoxDecoration(
                    color: Color(0xFF272A4E),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.mood,
                        size: 150,
                      ),
                      Text("Happy"),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: FlatButton(
                onPressed: () {},
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 0,
                  ),
                  margin: EdgeInsets.all(0),
                  decoration: BoxDecoration(
                    color: Color(0xFF272A4E),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.mood_bad,
                        size: 150,
                      ),
                      Text("Sad"),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
      ],
    );
  }
}
