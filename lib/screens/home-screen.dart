import 'package:flutter/material.dart';
import './body.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        primaryColor: Color(0xFF090C22),
        accentColor: Color(0xFFFF0067),
        scaffoldBackgroundColor: Color(0xFF090C22),
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.sort),
          title: Center(
            child: Text(
              "BMI App",
            ),
          ),
          elevation: 10,
        ),
        body: Body(),
      ),
    );
  }
}
