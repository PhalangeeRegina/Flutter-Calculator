import 'package:flutter/material.dart';
import './screens/home-screen.dart';

void main() {
  runApp(BMIApp());
}

class BMIApp extends StatefulWidget {
  @override
  _BMIAppState createState() => _BMIAppState();
}

class _BMIAppState extends State<BMIApp> {
  @override
  Widget build(BuildContext context) {
    return HomeScreen();
  }
}
